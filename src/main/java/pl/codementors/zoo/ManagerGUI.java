package pl.codementors.zoo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by psysiu on 7/8/17.
 */
public class ManagerGUI extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("ZOO");
        Parent root = FXMLLoader.load(getClass().getResource("/pl/codementors/zoo/fxml/manager.fxml"));
        Scene scene = new Scene(root, 600, 400);
        stage.setScene(scene);
        stage.show();
    }

}
