package pl.codementors.zoo.model;

import lombok.*;

/**
 * Created by psysiu on 6/29/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Breeder {

    public enum Position {
        junior, senior;
    }

    private int id;

    private String name;

    private String surname;

    private Position position;

    public Breeder(String name, String surname, Position position) {
        this.name = name;
        this.surname = surname;
        this.position = position;
    }
}
