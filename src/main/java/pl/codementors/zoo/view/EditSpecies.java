package pl.codementors.zoo.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.Setter;
import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Species;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by psysiu on 7/8/17.
 */
public class EditSpecies implements Initializable {

    private Species species;

    private SpeciesDAO speciesDAO = new SpeciesDAO();

    public EditSpecies(Species species) {
        this.species = species;
    }

    @FXML
    private TextField nameText;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nameText.setText(species.getName());
    }

    @FXML
    public void save(ActionEvent event) {
        species.setName(nameText.getText());
        speciesDAO.update(species);
        cancel(event);
    }

    @FXML
    public void cancel(ActionEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }
}
