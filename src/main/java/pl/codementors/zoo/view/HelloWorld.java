package pl.codementors.zoo.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by psysiu on 7/8/17.
 */
public class HelloWorld implements Initializable {

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private TextField input1;

    @FXML
    private TextField input2;

    @FXML
    public void confirm(ActionEvent event) {
        new Alert(Alert.AlertType.INFORMATION, "You entered: " + input1.getText()).showAndWait();
        new Alert(Alert.AlertType.INFORMATION, "You entered: " + input2.getText()).showAndWait();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are those the same");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK && input1.getText().equals(input2.getText())) {
            new Alert(Alert.AlertType.INFORMATION, ":)").show();
        } else {
            new Alert(Alert.AlertType.INFORMATION, ":(").show();
        }
    }

    @FXML
    public void helloButtonClicked(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Hello There!");
        alert.setContentText("Oh, hello.");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            new Alert(Alert.AlertType.INFORMATION, ":)").show();
        } else {
            new Alert(Alert.AlertType.INFORMATION, ":(").show();
        }
    }
}
