package pl.codementors.zoo.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Species;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;

/**
 * Created by psysiu on 7/8/17.
 */
@Log
public class Manager implements Initializable {

    private SpeciesDAO speciesDAO = new SpeciesDAO();

    private ObservableList<Species> species = FXCollections.observableArrayList();

    @FXML
    private TableView<Species> speciesTable;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        speciesTable.setItems(species);
    }

    @FXML
    private void loadSpecies(ActionEvent event) {
        species.clear();
        species.addAll(speciesDAO.findALl());
    }

    @FXML
    private void deleteSpecies(ActionEvent event) {
        Species species = speciesTable.getSelectionModel().getSelectedItem();
        if (species != null) {
            Optional<ButtonType> result  = new Alert(Alert.AlertType.CONFIRMATION, "Sure?").showAndWait();
            if (result.get() == ButtonType.OK) {
                speciesDAO.delete(species);
                loadSpecies(event);
            }
        }
    }

    @FXML
    private void editSpecies(ActionEvent event) {
        Species species = speciesTable.getSelectionModel().getSelectedItem();
        if (species != null) {
            Stage stage = new Stage();
            stage.setTitle("Edit species");
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/pl/codementors/zoo/fxml/edit_species.fxml"));
                EditSpecies editSpecies = new EditSpecies(species);
                loader.setController(editSpecies);
                Parent root = loader.load();
                Scene scene = new Scene(root, 600, 400);
                stage.setScene(scene);
                stage.showAndWait();
                loadSpecies(event);
            } catch (IOException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }

        }
    }

}
